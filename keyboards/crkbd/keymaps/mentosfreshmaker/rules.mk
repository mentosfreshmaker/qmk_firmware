# Bootloader selection
BOOTLOADER = atmel-dfu

# enable OLED displays
OLED_DRIVER_ENABLE = yes

# enable media keys
EXTRAKEY_ENABLE = yes

# enable LEDs
RGBLIGHT_ENABLE = yes

# enable mouse keys
MOUSEKEY_ENABLE = yes