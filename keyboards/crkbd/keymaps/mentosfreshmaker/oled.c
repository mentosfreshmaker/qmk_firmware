#pragma once

extern uint8_t is_master;

oled_rotation_t oled_init_user(oled_rotation_t rotation) {
  // align both vertically
  if (is_master) {
    return OLED_ROTATION_180;
  }
  return OLED_ROTATION_270;
}

void render_layer_state(void) {
    oled_write_ln_P(PSTR("LAYER"), false);
    oled_write_ln_P(PSTR("BASE"), layer_state_is(_BASE));
    oled_write_ln_P(PSTR("NUM"), layer_state_is(_NUM));
    oled_write_ln_P(PSTR("GAME"), layer_state_is(_GAME));
    oled_write_ln_P(PSTR("ADJ"), layer_state_is(_ADJUST));
    oled_write_ln_P(PSTR("\n"), false);
}

void render_bootmagic_status(void) {
    /* Show Ctrl-Gui Swap options */
    oled_write_ln_P(PSTR("OS"), false);
    oled_write_ln_P(PSTR("WIN"), !keymap_config.swap_lctl_lgui);
    oled_write_ln_P(PSTR("MAC"), keymap_config.swap_lctl_lgui);
    oled_write_ln_P(PSTR("\n"), false);
}

char keylog_str[24] = {};

const char code_to_name[60] = {
    ' ', ' ', ' ', ' ', 'a', 'b', 'c', 'd', 'e', 'f',
    'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p',
    'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
    '1', '2', '3', '4', '5', '6', '7', '8', '9', '0',
    'R', 'E', 'B', 'T', '_', '-', '=', '[', ']', '\\',
    '#', ';', '\'', '`', ',', '.', '/', ' ', ' ', ' '};

void set_keylog(uint16_t keycode, keyrecord_t *record) {
  char name = ' ';
    if ((keycode >= QK_MOD_TAP && keycode <= QK_MOD_TAP_MAX) ||
        (keycode >= QK_LAYER_TAP && keycode <= QK_LAYER_TAP_MAX)) { keycode = keycode & 0xFF; }
  if (keycode < 60) {
    name = code_to_name[keycode];
  }

  // update keylog
  snprintf(keylog_str, sizeof(keylog_str), "CHAR\n%c", name);
}

void render_keylog(void) {
    oled_write(keylog_str, false);
}

void render_logo(void) {
    static const char PROGMEM crkbd_logo[] = {
        0x80, 0x81, 0x82, 0x83, 0x84, 0x85, 0x86, 0x87, 0x88, 0x89, 0x8a, 0x8b, 0x8c, 0x8d, 0x8e, 0x8f, 0x90, 0x91, 0x92, 0x93, 0x94,
        0xa0, 0xa1, 0xa2, 0xa3, 0xa4, 0xa5, 0xa6, 0xa7, 0xa8, 0xa9, 0xaa, 0xab, 0xac, 0xad, 0xae, 0xaf, 0xb0, 0xb1, 0xb2, 0xb3, 0xb4,
        0xc0, 0xc1, 0xc2, 0xc3, 0xc4, 0xc5, 0xc6, 0xc7, 0xc8, 0xc9, 0xca, 0xcb, 0xcc, 0xcd, 0xce, 0xcf, 0xd0, 0xd1, 0xd2, 0xd3, 0xd4,
        0};
    oled_write_P(crkbd_logo, false);
}

void oled_task_user(void) {
    if (is_master) {
        render_logo();
    } else {
        render_layer_state();
        render_bootmagic_status();
        render_keylog();
    }
}
