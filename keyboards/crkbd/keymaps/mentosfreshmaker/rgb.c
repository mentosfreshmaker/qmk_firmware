#pragma once

extern rgblight_config_t rgblight_config;
rgblight_config_t        RGB_current_config;

void save_rgb_config(void) {
    RGB_current_config.enable = rgblight_config.enable;
    RGB_current_config.mode   = rgblight_get_mode();
    RGB_current_config.speed  = rgblight_get_speed();
    RGB_current_config.hue    = rgblight_get_hue();
    RGB_current_config.sat    = rgblight_get_sat();
    RGB_current_config.val    = rgblight_get_val();
}

void rgb_by_layer(int layer) {
    rgblight_mode_noeeprom(RGBLIGHT_MODE_STATIC_LIGHT);

    switch (layer) {
        case _NUM:
            rgblight_sethsv_noeeprom(HSV_AZURE);
            break;
        case _GAME:
            rgblight_sethsv_noeeprom(HSV_PINK);
            break;
        case _ADJUST:
            rgblight_sethsv_noeeprom(HSV_WHITE);
            break;
        default:
            rgblight_sethsv_noeeprom(HSV_PURPLE);
    }
}
