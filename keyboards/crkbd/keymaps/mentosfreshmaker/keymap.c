/*
Copyright 2019 @foostan
Copyright 2020 Drashna Jaelre <@drashna>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include QMK_KEYBOARD_H

enum layers {
  _BASE,
  _NUM,
  _GAME,
  _ADJUST
};

#define MO_NUM MO(_NUM)
#define MACWIN CG_TOGG
#define TO_BASE TO(_BASE)
#define TO_GAME TO(_GAME)
#define TO_ADJUST TO(_ADJUST)
#define SCRNSHOT LGUI(KC_PSCR)
#define GTFO LCTL(LALT(KC_DEL))

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
  [_BASE] = LAYOUT_split_3x6_3(
  //,-----------------------------------------------------.                    ,-----------------------------------------------------.
      KC_ESC,    KC_Q,    KC_W,    KC_E,    KC_R,   KC_T,                         KC_Y,    KC_U,    KC_I,    KC_O,   KC_P,   KC_BSPC,
  //|--------+--------+--------+--------+--------+--------|                    |--------+--------+--------+--------+--------+--------|
      KC_TAB,    KC_A,    KC_S,    KC_D,   KC_F,    KC_G,                         KC_H,    KC_J,    KC_K,    KC_L, KC_SCLN, KC_QUOT,
  //|--------+--------+--------+--------+--------+--------|                    |--------+--------+--------+--------+--------+--------|
     TO_ADJUST,   KC_Z,   KC_X,    KC_C,    KC_V,    KC_B,                         KC_N,   KC_M,    KC_COMM, KC_DOT, KC_SLSH, KC_RGUI,
  //|--------+--------+--------+--------+--------+--------+--------|  |--------+--------+--------+--------+--------+--------+--------|
                                          MO_NUM,  KC_SPC,  KC_LCTL,   KC_RSFT,  KC_ENT,  KC_RALT
                                      //`--------------------------'  `--------------------------'

  ),

  [_NUM] = LAYOUT_split_3x6_3(
  //,-----------------------------------------------------.                    ,-----------------------------------------------------.
      GTFO, _______, _______,  KC_UP,  SCRNSHOT,KC_MS_WH_UP,                    KC_EQL,   KC_7,    KC_8,    KC_9,  _______, KC_DELETE,
  //|--------+--------+--------+--------+--------+--------|                    |--------+--------+--------+--------+--------+--------|
      KC_TAB, _______, KC_LEFT, KC_DOWN, KC_RGHT,KC_MS_WH_DOWN,                   KC_0,    KC_4,    KC_5,    KC_6,   KC_GRV, _______,
  //|--------+--------+--------+--------+--------+--------|                    |--------+--------+--------+--------+--------+--------|
      _______, _______, _______, KC_LBRC, KC_RBRC, _______,                      KC_MINS,  KC_1,    KC_2,    KC_3,   KC_BSLS, KC_CALC,
  //|--------+--------+--------+--------+--------+--------+--------|  |--------+--------+--------+--------+--------+--------+--------|
                                          KC_TRNS, KC_SPC,  KC_LCTL,    KC_RSFT, KC_ENT, KC_RALT
                                      //`--------------------------'  `--------------------------'
  ),

  
  [_ADJUST] = LAYOUT_split_3x6_3(
  //,-----------------------------------------------------.                    ,-----------------------------------------------------.
     _______, RGB_M_B, RGB_M_SW, KC_VOLU, TO_GAME, KC_MPRV,                      KC_F11, KC_PGUP, KC_MS_UP, _______, _______, _______,
  //|--------+--------+--------+--------+--------+--------|                    |--------+--------+--------+--------+--------+--------|
     _______, RGB_M_K,  RGB_M_R, KC_VOLD, TO_BASE, KC_MNXT,                    KC_MS_BTN5, KC_MS_LEFT, KC_MS_DOWN, KC_MS_RIGHT, _______, _______,
  //|--------+--------+--------+--------+--------+--------|                    |--------+--------+--------+--------+--------+--------|
     _______, RGB_M_X, RGB_M_SN, KC_MUTE, MACWIN, KC_MPLY,                     KC_MS_BTN4, KC_PGDN, _______, _______, _______, _______,
  //|--------+--------+--------+--------+--------+--------+--------|  |--------+--------+--------+--------+--------+--------+--------|
                                          _______, KC_SPC, KC_LCTL,    KC_MS_BTN3, KC_MS_BTN1, KC_MS_BTN2
                                      //`--------------------------'  `--------------------------'
  ),

  [_GAME] = LAYOUT_split_3x6_3(
  //,-----------------------------------------------------.                    ,-----------------------------------------------------.
      KC_ESC,   KC_5,    KC_4,    KC_W,    KC_3,    KC_R,                         KC_Y,    KC_U,    KC_I,    KC_O,   KC_P,  KC_BSPC,
  //|--------+--------+--------+--------+--------+--------|                    |--------+--------+--------+--------+--------+--------|
      KC_TAB,   KC_6,    KC_A,    KC_S,    KC_D,    KC_F,                          KC_H,   KC_J,    KC_K,   KC_L,   KC_SCLN, KC_QUOT,
  //|--------+--------+--------+--------+--------+--------|                    |--------+--------+--------+--------+--------+--------|
      _______,  KC_7,    KC_1,    KC_C,   KC_LSFT,  KC_B,                         KC_N,    KC_M,   KC_COMM, KC_DOT, KC_SLSH,  _______,
  //|--------+--------+--------+--------+--------+--------+--------|  |--------+--------+--------+--------+--------+--------+--------|
                                          KC_2,   KC_SPC,  KC_1,    KC_RSFT,  KC_ENT,  KC_RALT
                                      //`--------------------------'  `--------------------------'
  )

};

#ifdef OLED_DRIVER_ENABLE
#    include "oled.c"
#endif

#if defined(RGBLIGHT_ENABLE)
#    include "rgb.c"
#endif


#if defined(RGBLIGHT_ENABLE)
layer_state_t layer_state_set_user(layer_state_t state) {
    uint8_t layer = get_highest_layer(state);
    rgb_by_layer(layer);
    return state;
}
#endif

bool process_record_user(uint16_t keycode, keyrecord_t *record) {
  if (record->event.pressed) {
    set_keylog(keycode, record);
  }
  return true;
}